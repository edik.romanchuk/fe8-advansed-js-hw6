const Button = document.querySelector('button');

Button.addEventListener('click', findIP)

async function findIP() {
    const {ip} = await fetch('https://api.ipify.org/?format=json')
     .then(response => response.json())

     const{continent, country, region, city, district} = await fetch(`http://ip-api.com/json/${ip}`)
        .then(response => response.json())

    Button.insertAdjacentHTML('afterend', 
    `<div>Continent: ${continent}</div>
    <div>Country: ${country}</div>
    <div>Region: ${region}</div>
    <div>City: ${city}</div>
    <div>District: ${district}</div>`)    
}